import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */

/** 
 * fungsi affix untuk membuat tab permanen
*/

export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: { title: 'Dashboard', icon: 'dashboard', affix: true }
      }
    ]
  },

  
  {
    path: '/pendaftaran-pasien',
    component: Layout,
    children: [
      {
        path: 'pendaftaran-pasien',
        component: () => import('@/views/rs/pendaftaran-pasien'),
        name: 'pendaftaran-pasien',
        meta: { title: 'Pendaftaran Pasien', icon: 'form', affix: false }
      }
    ]
  },
  {
    path: '/lab-klinik',
    component: Layout,
    children: [
      {
        path: 'lab-klinik',
        component: () => import('@/views/rs/labklinik'),
        name: 'lab-klinik',
        meta: { title: 'Lab Klinik', icon: 'el-icon-first-aid-kit', affix: false }
      }
    ]
  },

  {
    path: '/kat-kesehatan',
    component: Layout,
    children: [
      {
        path: 'lab-klinik',
        component: () => import('@/views/rs/kategori-kesehatan'),
        name: 'Kategori-Kesehatan',
        meta: { title: 'Kategori Kesehatan', icon: 'component', affix: false }
      }
    ]
  },

  {
    path: '/permintaan-radiologi',
    component: Layout,
    children: [
      {
        path: 'permintaan-radiologi',
        component: () => import('@/views/rs/permintaan-radiologi'),
        name: 'permintaan-radiologi',
        meta: { title: 'Permintaan Radiologi', icon: 'el-icon-receiving', affix: false }
      }
    ]
  },

  {
    path: '/persetujuan-tindakan-medis-KB',
    component: Layout,
    children: [
      {
        path: 'persetujuan-tindakan-medis-KB',
        // component: () => import('@/views/rs/kunjungan-ulang'),
        name: 'persetujuan-tindakan-medis-KB',
        meta: { title: 'Persetujuan Tindakan KB', icon: 'el-icon-first-aid-kit', affix: false }
      }
    ]
  },

  {
    path: '/pernyataan-tindakan-medis',
    component: Layout,
    children: [
      {
        path: 'pernyataan-tindakan-medis',
        // component: () => import('@/views/rs/kunjungan-ulang'),
        name: 'pernyataan-tindakan-medis',
        meta: { title: 'Pernyataan Medis', icon: 'el-icon-first-aid-kit', affix: false }
      }
    ]
  },

  {
    path: '/kunjungan-ulang',
    component: Layout,
    children: [
      {
        path: 'kunjungan-ulang',
        // component: () => import('@/views/rs/kunjungan-ulang'),
        name: 'kunjungan-ulang',
        meta: { title: 'Kunjungan Ulang', icon: 'skill', affix: false }
      }
    ]
  },

  {
    path: '/biaya-rawat-inap',
    component: Layout,
    children: [
      {
        path: 'biaya-rawat-inap',
        // component: () => import('@/views/rs/kunjungan-ulang'),
        name: 'biaya-rawat-inap',
        meta: { title: 'Biaya Rawat inap', icon: 'el-icon-wallet', affix: false }
      }
    ]
  },
  {
    path: '/kwitansi-rincian-biaya',
    component: Layout,
    children: [
      {
        path: 'kwitansi-rincian-biaya',
        // component: () => import('@/views/rs/kunjungan-ulang'),
        name: 'kwitansi-rincian-biaya',
        meta: { title: 'Rincian Biaya', icon: 'el-icon-s-finance', affix: false }
      }
    ]
  },

  {
    path: '/catatan-instruksi',
    component: Layout,
    // redirect: '/example/list',
    name: 'catatan-instruksi',
    meta: {
      title: 'catatan instruksi',
      // icon: 'el-icon-s-help'
      icon: 'education'
    },
    children: [
      {
        path: 'dokter',
        // component: () => import('@/views/example/create'),
        name: 'dokter',
        meta: { title: 'Instruksi Dokter', icon: 'edit' }
      },
      {
        path: 'perawat',
        // component: () => import('@/views/example/create'),
        name: 'perawat',
        meta: { title: 'Perawat', icon: 'edit' }
      }
    ]
  },

  {
    path: '/catatan-kesehatan',
    component: Layout,
    // redirect: '/example/list',
    name: 'catatan-kesehatan',
    meta: {
      title: 'catatan kesehatan',
      // icon: 'el-icon-s-help'
      icon: 'education'
    },
    children: [
      {
        path: 'ibu-hamil-petugas',
        // component: () => import('@/views/example/create'),
        name: 'ibu-hamil-petugas',
        meta: { title: 'Petugas Ibu Hamil', icon: 'user' }
      },
      {
        path: 'hasil-pelayanan-ibu-nifas',
        // component: () => import('@/views/example/create'),
        name: 'hasil-pelayanan-ibu-nifas',
        meta: { title: 'Hasil Pelayanan', icon: 'user' }
      },
      {
        path: 'ibu-hamil',
        // component: () => import('@/views/example/create'),
        name: 'ibu-hamil',
        meta: { title: 'Ibu Hamil', icon: 'peoples' }
      },
      {
        path: 'ibu-bersalin',
        // component: () => import('@/views/example/create'),
        name: 'ibu-bersalin',
        meta: { title: 'Ibu Bersalin', icon: 'peoples' }
      },
      {
        path: 'ibu-nifas',
        // component: () => import('@/views/example/create'),
        name: 'ibu-nifas',
        meta: { title: 'Ibu Nifas', icon: 'peoples' }
      },
    ]
  },

  {
    path: '/daftar-monitor',
    component: Layout,
    // redirect: '/example/list',
    name: 'daftar-monitor',
    meta: {
      title: 'daftar monitor',
      // icon: 'el-icon-s-help'
      icon: 'el-icon-odometer'
    },
    children: [
      {
        path: 'daftar-monitor-infus',
        // component: () => import('@/views/example/create'),
        name: 'daftar-monitor-infus',
        meta: { title: 'Daftar monitor infus', icon: 'edit' }
      },
      {
        path: 'daftar-pemakaian-obat-pasien',
        // component: () => import('@/views/example/create'),
        name: 'daftar-pemakaian-obat-pasien',
        meta: { title: 'Daftar Pemakaian Obat', icon: 'edit' }
      }
    ]
  },

  {
    path: '/konsultasi',
    component: Layout,
    // redirect: '/example/list',
    name: 'konsultasi',
    meta: {
      title: 'Konsultasi',
      // icon: 'el-icon-s-help'
      icon: 'component'
    },
    children: [
      {
        path: 'formulir-konsult-internal',
        // component: () => import('@/views/example/create'),
        name: 'formulir-konsult-internal',
        meta: { title: 'Konsultasi Internal', icon: 'edit' }
      },
      {
        path: 'formulir-konsult-external',
        // component: () => import('@/views/example/create'),
        name: 'formulir-konsult-external',
        meta: { title: 'Konsultasi External', icon: 'edit' }
      },
      {
        path: 'formulir-penggerakan-pemantapan-KIE',
        // component: () => import('@/views/example/create'),
        name: 'formulir-penggerakan-pemantapan-KIE',
        meta: { title: 'Konsultasi KIE KB', icon: 'edit' }
      }
    ]
  },
  {
    path: '/kartu KB',
    component: Layout,
    // redirect: '/example/list',
    name: 'kartu KB',
    meta: {
      title: 'kartu KB',
      // icon: 'el-icon-s-help'
      icon: 'el-icon-postcard'
    },
    children: [
      {
        path: 'kartu-peserta-kb',
        // component: () => import('@/views/example/create'),
        name: 'kartu-peserta-kb',
        meta: { title: 'Kartu Peserta KB', icon: 'edit' }
      },
      {
        path: 'status-peserta-kb',
        // component: () => import('@/views/example/create'),
        name: 'status-peserta-kb',
        meta: { title: 'Status Peserta KB', icon: 'edit' }
      }
    ]
  },

  {
    path: '/surat-rujukan',
    component: Layout,
    // redirect: '/example/list',
    name: 'surat-rujukan',
    meta: {
      title: 'Surat Rujukan',
      // icon: 'el-icon-s-help'
      icon: 'form'
    },
    children: [
      {
        path: 'rujukan-perawatan',
        // component: () => import('@/views/example/create'),
        name: 'rujukan-perawatan',
        meta: { title: 'Rujukan Perawatan', icon: 'edit' }
      },
      {
        path: 'rujukan-pemerikasaan-FKTP',
        // component: () => import('@/views/example/create'),
        name: 'rujukan-pemerikasaan-FKTP',
        meta: { title: 'Pemeriksaan FKTP', icon: 'edit' }
      }
    ]
  },

  {
    path: '/icon',
    component: Layout,
    children: [
      {
        path: 'index',
        component: () => import('@/views/icons/index'),
        name: 'Icons',
        meta: { title: 'Icons', icon: 'icon', noCache: true }
      }
    ]
  },

  
  
]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}
export default router
